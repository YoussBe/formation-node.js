/*

Exercice I 

Créer une liste d'objets "eleves" ayant la propriété nom : chaines de caracteres, prenom : chaines de caracteres et presence : booléan
créer une fonction qui sépare la liste "eleves" en deux listes : eleves_presents et eleves_absents , selon la valeure de presence.
*/

/*

Exercice II 

Créer une liste d'objets "stagiaires" heritant de la classe Eleve ayant la propriété booléenne "fin_stage"
créer une fonction qui sépare la liste "stagiaires" en deux listes : stagiaires_validés et stagiaires_non_validés , selon les valeure de presence et de fin_stage.
*/