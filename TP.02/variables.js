/*

Exercice I

créer les variables suivantes : 

number qui contient le nombre 42
text qui contient la chaîne de caractère 199
bin qui contient la valeur booléenne fausse
real qui contient la valeur 3,14

*/


console.log(number);
console.log(text);
console.log(bin);
console.log(real);


/*

Exercice 2

Appliquez à number l'opérateur d'incrémentation
Concaténez à la chaîne text la chaîne " est ici une chaîne de caractères"
Inversez la valeur logique de bin
Ajoutez à real la valeur contenue dans num

*/

console.log(number);
console.log(text);
console.log(bin);
console.log(real);

/*
Exercice 3

Convertir la chaîne contenue dans text en nombre avec parseInt() et mettre le resultat dans une nouvelle variable
ajouter à number le contenu de la variable */

console.log(number);

/*
executer le code avec node 'chemin/vers/leFichier.js' */


