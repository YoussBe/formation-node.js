/*

Exercice I

créer une fonction iAmHere qui renvoie la chaine "Oui" 


*/

console.log(iAmHere());

/*

Exercice II

créer une fonction iAmHere qui prend un argument nom et qui  renvoie la chaine "Oui je suis " + nom 


*/

/*

Exercice II

créer une fonction qui teste si la chaine "Oui je suis " + nom a une longeur qui dépasse 10 caractères : renvoie vraie si c'est le cas et faux sinon. 
str.length est utilisé pour connaitre la longeur des chaines.

*/

/*

Exercice III

créer une fonction qui teste si l'argument nom est une chaine de caractères renvoie  "Oui je suis " + nom  dans ce cas et "non je suis pas une personne" dans le cas contraire. 
utliser typeof()
*/

/*

Exercice IV

Créez une fonction :

créer une fonction iAmHere qui prend un argument un chaine nom
renvoie "trés court" si la longeur du nom est inférieure à 3
renvoie "court" si la longeur du nom est comprise entre 3 et 5 inclus 
renvoie "Acceptable" si la longeur du nom est comprise entre 6 et 12 inclus 
renvoie "long" si la longeur du nom est supérieure à 12

*/