/* 
Ex I
à l'aide du module "http" et de la methode createServer , afficher dans le navigateur dans http://localhost:8080 le message "J'ai lancé mon premier serveur NodeJs en quelques ligne !" 


Ex II

à l'aide de l'argument req, dans createServer afficher la partie de l''url qui vient aprés http://localhost:8080
ex : si on accéde au http://localhost:8080/first-page , le navigateur devra afficher /first-page


Ex III

afficher "Vous etes dans la page principale" dans le navigateur, si on navige à l'url http://localhost:8080/home

Ex IV

à l'aide du module "url" et de la methode parse(), afficher le nom et prenom et la taille de la chaine qui les compose dans le navigateur si on navige à l'url http://localhost:8080/?familyname=nom&firstname=prenom

*/
